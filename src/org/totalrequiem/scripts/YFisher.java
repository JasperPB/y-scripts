package org.totalrequiem.scripts;

import org.powerbot.script.Filter;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;
import org.powerbot.script.rt4.Item;
import org.totalrequiem.core.Action;
import org.totalrequiem.core.ActionScript;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.impl.Completable;
import org.totalrequiem.core.task.Task;
import org.totalrequiem.core.task.pre.prim.ClickInterface;
import org.totalrequiem.core.task.pre.prim.Interact;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 3/4/15
 */

@Script.Manifest(description = "Powerfishes with small fishing net", name = "YNetFisher")
public class YFisher extends ActionScript {


    public YFisher() {
        super(true);
    }

    @Override
    public Action[] actions() {
        return new Action[] {new Fish(ctx), new Drop(ctx)};
    }


    public class Drop extends Action {

        public Drop(Context ctx) {
            super(ctx);
        }

        @Override
        public Task[] tasks() {
            final Task drop = new Task(ctx) {
                @Override
                public boolean completed() {
                    return ctx.inventory.select().select(new Filter<Item>() {

                        @Override
                        public boolean accept(Item item) {
                            return !item.name().equals("Small fishing net");
                        }
                    }).count() == 0;
                }

                @Override
                public void run() {
                    for(int i = 0; i < 4; i++) {
                        for(int i2 = 0; i2 < 7; i2++) {
                            final int spot = i + i2 * 4;
                            final Item item = ctx.inventory.itemAt(spot);
                            if(item.name().contains("net")) {
                                continue;
                            }
                            if(!item.equals(ctx.inventory.nil())) {
                                item.interact("Drop");
                            }
                        }
                    }
                }
            };
            return new Task[] {drop};
        }

        @Override
        public boolean completed() {
            return ctx.inventory.select().select(new Filter<Item>() {

                @Override
                public boolean accept(Item item) {
                  return !item.name().equals("Small fishing net");
                }
            }).count() == 0;
        }
    }

    public class Fish extends Action {

        public Fish(final Context ctx) {
            super(ctx);
        }


        @Override
        public Task[] tasks() {
            final Task pickup = new Interact(ctx, "Small fishing net", "Take", Interact.GROUNDITEM, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.inventory.select().select(new Filter<Item>() {

                        @Override
                        public boolean accept(Item item) {
                            return item.name().contains("net");
                        }
                    }).count() > 0;
                }
            });

            final Task fish = new Interact(ctx, "Fishing Spot", "Net", Interact.NPC, new Completable() {
                @Override
                public boolean completed() {
                    return ctx.players.local().animation() != -1;
                }
            });

            final Task cont = new ClickInterface(ctx, "continue");


            return new Task[] {fish, cont};
        }

        @Override
        public boolean completed() {
            return ctx.inventory.select().count() == 28;
        }
    }
}
