package org.totalrequiem.core.context.ext;

import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.context.Provider;
import org.totalrequiem.core.task.Task;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class ExecutorQueue extends Provider {

    private final Deque<Task> queue;

    /**
     * This class' constructor which initializes the Queue
     *
     * @param ctx the context used in this script instance
     */
    public ExecutorQueue(final Context ctx) {
        super(ctx);
        queue = new LinkedList<>();
    }

    /**
     * Gets the top value of the queue
     *
     * @return the Task on top of the queue
     */
    public Task peek() {
        return queue.peek();
    }

    /**
     * Creates an array from all elements in the queue
     *
     * @return a Task array
     */
    public Task[] list() {
        return queue.toArray(new Task[queue.size()]);
    }

    /**
     * Removes the top value from the queue
     *
     * @return true if the value was removed successfully
     */
    public boolean remove() {
        return queue.remove(peek());
    }

    /**
     * Adds one or more tasks to the queue
     *
     * @param tasks the task(s) to add
     */
    public void offer(final Task... tasks) {
        for (int i = tasks.length - 1; i >= 0; i--) {
            queue.addFirst(tasks[i]);
        }
    }

    /**
     * Adds one or more tasks to the end of the queue if end equals true
     *
     * @param end   whether or not to add the task(s) to the end oof the queue
     * @param tasks the task(s) to add
     */
    public void offer(final boolean end, final Task... tasks) {
        if (!end) offer(tasks);
        for (int i = tasks.length - 1; i >= 0; i--) {
            queue.addLast(tasks[i]);
        }
    }
}

