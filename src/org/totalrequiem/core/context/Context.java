package org.totalrequiem.core.context;

import org.powerbot.script.rt4.ClientContext;
import org.totalrequiem.core.context.ext.ExecutorQueue;
import org.totalrequiem.core.context.ext.Utilities;
import org.totalrequiem.core.obs.EventMonitor;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class Context extends ClientContext {

    /**
     * The ExecutorQueue instance initialized in the Context constructor
     */
    public ExecutorQueue executor;

    /**
     * The EventMonitor instance initialized in the Context constructor
     */
    public EventMonitor monitor;

    /**
     * The Utilities instance initialized in the Context constructor
     */
    public Utilities util;

    /**
     * The constructor for this Context
     * @param ctx the context used to create this context
     */
    public Context(final ClientContext ctx) {
        super(ctx);
        this.executor = new ExecutorQueue(this);
        this.monitor = new EventMonitor(this);
        this.util = new Utilities(this);
    }
}
