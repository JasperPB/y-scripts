package org.totalrequiem.core;

import org.powerbot.script.PollingScript;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.task.Task;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 3/4/15
 */
public abstract class ActionScript extends PollingScript<Context> {

    private final boolean repeat;

    public ActionScript(final boolean repeat) {
        this.repeat = repeat;
    }

    @Override
    public void start() {
        ctx.executor.offer(actions());
    }

    public abstract Action[] actions();

    @Override
    public void poll() {
        final Task current = ctx.executor.peek();
        if (current != null) {
            if (current.completed()) {
                if(repeat && current instanceof Action) {
                    ctx.executor.offer(true, current);
                }
                ctx.executor.remove();
            } else {
                current.run();
            }
        }

    }
}
