package org.totalrequiem.core.task.pre.prim;

import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.impl.Completable;
import org.totalrequiem.core.task.Task;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class SleepTask extends Task{

    private final long millis;
    private final Completable completed;
    private boolean complete = false;

    public SleepTask(final Context ctx, final long millis, final Completable completed) {
        super(ctx);
        this.millis = millis;
        this.completed = completed;
    }

    public SleepTask(final Context ctx, final long millis) {
        this(ctx, millis, new Completable() {
            @Override
            public boolean completed() {
                return false;
            }
        });
    }

    @Override
    public boolean completed() {
        return complete;
    }

    @Override
    public void run() {
        ctx.util.sleepFor(millis, completed);
        complete = true;
    }
}
