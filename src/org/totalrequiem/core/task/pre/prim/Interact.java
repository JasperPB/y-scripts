package org.totalrequiem.core.task.pre.prim;

import org.powerbot.script.Identifiable;
import org.powerbot.script.Locatable;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.BasicQuery;
import org.powerbot.script.rt4.Interactive;
import org.powerbot.script.rt4.Item;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.impl.Completable;
import org.totalrequiem.core.task.Task;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class Interact extends Task {

    public final static int NPC = 0;
    public final static int OBJECT = 1;
    public final static int GROUNDITEM = 2;
    public final static int INVENTORY = 3;
    private final String name;
    private final String action;
    private final int type;
    private final Completable condition;
    private final boolean right;
    private final Tile location;


    public Interact(final Context ctx, final String name, final String action, final int type, final Completable condition, final boolean right) {
        super(ctx);
        this.name = name;
        this.action = action;
        this.type = type;
        this.condition = condition;
        this.right = right;
        this.location = null;

    }

    public Interact(final Context ctx, final String name, final String action, final int type, final Completable condition) {
        this(ctx, name, action, type, condition, false);
    }

    public Interact(final Context ctx, final String name, final String action, final int type) {
        this(ctx, name, action, type, new Completable() {
            @Override
            public boolean completed() {
                return ctx.players.local().animation() != -1;
            }
        });
    }

    public Interact(final Context ctx, final Tile location, final String action, final int type, final Completable condition, boolean right) {
        super(ctx);
        this.name = "";
        this.action = action;
        this.type = type;
        this.condition = condition;
        this.right = right;
        this.location = location;
    }

    public Interact(final Context ctx, final Tile location, final String action, final int type, Completable condition) {
        this(ctx, location, action, type, condition, false);
    }

    public Interact(final Context ctx, final Tile location, final String action, final int type) {
        this(ctx, location, action, type, new Completable() {
            @Override
            public boolean completed() {
                return ctx.players.local().animation() != -1;
            }
        });
    }

    @Override
    public void run() {
        final BasicQuery<? extends Interactive> query;

        switch (type) {
            case NPC:
                query = ctx.npcs;
                break;

            case OBJECT:
                query = ctx.objects;
                break;

            case GROUNDITEM:
                query = ctx.groundItems;
                break;

            case INVENTORY:
                System.out.println(ctx.inventory);
                final Item item = ctx.inventory.select().name(name).peek();
                if (item.interact(action)) {
                    ctx.util.sleepFor(1200, condition);
                }
                return;

            default:
                query = null;
                break;
        }

        final Interactive target = name == "" ? query.select().at(location).nearest().peek() : query.select().name(name).nearest().peek();
        System.out.println("Test");
        //System.out.println(ctx.inventory.selectedItem().id() + ", " + ctx.inventory.nil());
        if (right) {
            ctx.camera.pitch(true);
        } else {
            ctx.camera.pitch(false);
            ctx.camera.turnTo((Locatable) target);
        }

        if (!ctx.players.local().inCombat() && ctx.players.local().animation() == -1 && target.interact(!right, action)) {
            ctx.util.sleep(1000);
            ctx.util.sleepFor(1200, condition);
        }
    }

    @Override
    public boolean completed() {
        return condition.completed();
    }

}
