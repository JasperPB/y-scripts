package org.totalrequiem.core.task.pre.prim;

import org.powerbot.script.Tile;
import org.powerbot.script.rt4.LocalPath;
import org.powerbot.script.rt4.Player;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.impl.Completable;
import org.totalrequiem.core.task.Task;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class WalkPathTowards extends Task {

    private final Tile tile;
    private final LocalPath path;

    public WalkPathTowards(final Context ctx, final Tile tile) {
        super(ctx);
        this.tile = tile;
        this.path = ctx.movement.findPath(tile);
    }

    @Override
    public boolean completed() {
        return ctx.players.local().tile().distanceTo(tile) < 5;
    }

    @Override
    public void run() {
        if(!ctx.movement.running()) ctx.movement.running(true);
        if(path.traverse()) {
            ctx.util.sleepFor(750, new Completable() {
                @Override
                public boolean completed() {
                    final Player local = ctx.players.local();
                    return !local.inMotion() || ctx.movement.destination().distanceTo(local) < 4;
                }
            });
        }
    }
}
