package org.totalrequiem.core.task.pre.prim;

import org.powerbot.script.rt4.Component;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.impl.Completable;
import org.totalrequiem.core.task.Task;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class ClickInterface extends Task {

    private final String text;
    private final int widget;
    private final int component;
    private final Completable completed;

    public ClickInterface(final Context ctx, final String text) {
        this(ctx, text, new Completable() {
            @Override
            public boolean completed() {
                final Component c = ctx.util.fromString(text);
                return c == null || !c.valid()|| !c.visible();
            }
        });
    }

    public ClickInterface(final Context ctx, final String text, final Completable completed) {
        super(ctx);
        this.text = text;
        this.widget = -1;
        this.component = -1;
        this.completed = completed;
    }

    public ClickInterface(final Context ctx, final int widget, final int component, final Completable completed) {
        super(ctx);
        this.text = "";
        this.widget = widget;
        this.component = component;
        this.completed = completed;
    }

    public ClickInterface(final Context ctx, final int widget, final int component) {
        this(ctx, widget,component, new Completable() {
            @Override
            public boolean completed() {
                final Component comp = ctx.widgets.component(widget, component);
                return !comp.visible();
            }
        });
    }



    @Override
    public void run() {
        final Component comp;
        if (text.equals("")) {
            comp = ctx.widgets.component(widget, component);
        } else {
            comp = ctx.util.fromString(text);
        }
        if (comp.valid() && comp.visible()) {
            if (comp.click()) {
                ctx.util.sleepFor(500, new Completable() {
                    @Override
                    public boolean completed() {
                        return !comp.valid() || !comp.visible();
                    }
                });
            }
        }
    }

    @Override
    public boolean completed() {
        return completed.completed();
    }

    @Override
    public int hashCode() {
        return text.hashCode();
    }
}
