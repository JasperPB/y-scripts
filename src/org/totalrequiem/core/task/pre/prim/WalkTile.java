package org.totalrequiem.core.task.pre.prim;

import org.powerbot.script.Tile;
import org.powerbot.script.rt4.Player;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.impl.Completable;
import org.totalrequiem.core.task.Task;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class WalkTile extends Task {

    private final Tile destination;
    private final boolean force;

    public WalkTile(final Context ctx, final Tile destination, final boolean force) {
        super(ctx);
        this.destination = destination;
        this.force = force;
    }

    public WalkTile(final Context ctx, final Tile destination) {
        this(ctx, destination, false);
    }

    @Override
    public boolean completed() {
        return ctx.players.local().tile().distanceTo(destination) < 5;
    }

    @Override
    public void run() {
        if(!ctx.movement.running()) ctx.movement.running(true);

        if(ctx.movement.step(destination)) {
            ctx.util.sleepFor(750, new Completable() {
                @Override
                public boolean completed() {
                    final Player local = ctx.players.local();
                    return !local.inMotion() || ctx.movement.destination().distanceTo(local) < 4;
                }
            });
        }
    }
}
