package org.totalrequiem.core.task.pre.tab;

import org.powerbot.script.rt4.Magic;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.task.Task;
import org.totalrequiem.core.task.pre.prim.ClickInterface;
import org.totalrequiem.core.task.pre.prim.Interact;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class CastSpell extends Task {

    private final Magic.Spell spell;
    private final String npc;
    private boolean complete;

    public CastSpell(final Context ctx, Magic.Spell spell, final String npc) {
        super(ctx);
        this.spell = spell;
        this.npc = npc;
    }

    @Override
    public boolean completed() {
        return complete;
    }

    @Override
    public void run() {
        ctx.executor.offer(new ClickInterface(ctx, ctx.magic.book().widget(), spell.component()),
                new Interact(ctx, npc, "Cast", Interact.NPC));
    }
}
