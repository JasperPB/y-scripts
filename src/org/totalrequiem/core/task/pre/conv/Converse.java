package org.totalrequiem.core.task.pre.conv;

import org.powerbot.script.rt4.Component;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.task.Task;
import org.totalrequiem.core.task.pre.prim.ClickInterface;

import java.util.HashSet;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class Converse extends Task {

    private final String[] options;
    private final HashSet<Task> taskSet = new HashSet<>();

    public Converse(final Context ctx, final String... options) {
        super(ctx);
        this.options = options;
        for(final String option : options) {
            final String temp = option.equals("continue") ? "Click here to continue" : option;
            taskSet.add(new ClickInterface(ctx, temp));
        }
    }

    @Override
    public void run() {
        ctx.executor.offer(taskSet.toArray(new Task[taskSet.size()]));
    }

    @Override
    public boolean completed() {
        final Component comp = ctx.util.fromStrings(options);
        return comp == null  || !comp.valid() || !comp.visible();
    }
}
