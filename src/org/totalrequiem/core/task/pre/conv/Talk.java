package org.totalrequiem.core.task.pre.conv;

import org.powerbot.script.rt4.Component;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.impl.Completable;
import org.totalrequiem.core.task.Task;
import org.totalrequiem.core.task.pre.prim.Interact;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class Talk extends Task {

    private final Task interact, converse;
    private final Completable condition;

    public Talk(final Context ctx, final String name, final Completable condition, final String... options) {
        super(ctx);
        this.interact = new Interact(ctx, name, "Talk", Interact.NPC, new Completable() {
            @Override
            public boolean completed() {
                final Component c = ctx.util.fromStrings(options);
                return c != null && c.valid() && c.visible();
            }
        });
        this.converse = new Converse(ctx, options);
        this.condition = condition;
    }

    @Override
    public boolean completed() {
        return condition.completed();
    }

    @Override
    public void run() {
        ctx.executor.offer(interact, converse);
    }
}
