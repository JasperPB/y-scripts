package org.totalrequiem.core.task.pre.inv;

import org.powerbot.script.rt4.Game;
import org.powerbot.script.rt4.Item;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.impl.Completable;
import org.totalrequiem.core.task.Task;
import org.totalrequiem.core.task.pre.prim.Interact;
import org.totalrequiem.core.task.pre.prim.SleepTask;
import org.totalrequiem.core.task.pre.tab.OpenTab;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class Use extends Task {

    private final String item1;
    private final String item2;
    private final Completable completed;

    public Use(final Context ctx, final String item1, final String item2, final Completable completad) {
        super(ctx);
        this.item1 = item1;
        this.item2 = item2;
        this.completed = completad;

    }

    public Use(final Context ctx, final String item1, final String item2) {
        this(ctx, item1, item2, new Completable() {
            @Override
            public boolean completed() {
                return ctx.players.local().animation() != -1;
            }
        });
    }

    @Override
    public boolean completed() {
        return completed.completed();
    }

    @Override
    public void run() {
        final Task[] tasks = {
                new OpenTab(ctx, Game.Tab.INVENTORY),
                new Interact(ctx, item1, "Use", Interact.INVENTORY, new Completable() {
                    @Override
                    public boolean completed() {
                        return ctx.inventory.select().name(item1).peek().contains(ctx.mouse.getLocation()) && ctx.menu.items()[0].equals("Cancel");
                    }
                }),
                new SleepTask(ctx, 500),
                new Interact(ctx, item2, "Use", Interact.INVENTORY, new Completable() {
            @Override
            public boolean completed() { return completed.completed();}
        }),
                new SleepTask(ctx, 1000, completed)};

        ctx.executor.offer(tasks);
    }
}
