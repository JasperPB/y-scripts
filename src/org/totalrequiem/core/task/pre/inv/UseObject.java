package org.totalrequiem.core.task.pre.inv;

import org.powerbot.script.rt4.Game;
import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.impl.Completable;
import org.totalrequiem.core.task.Task;
import org.totalrequiem.core.task.pre.prim.Interact;
import org.totalrequiem.core.task.pre.prim.SleepTask;
import org.totalrequiem.core.task.pre.tab.OpenTab;

/**
 * @author Jasper
 * @version: 1.0
 * @since: 8/6/14
 */
public class UseObject extends Task {

    private final String item;
    private final String object;
    private final Completable completed;

    public UseObject(final Context ctx, final String item, final String object, final Completable completed) {
        super(ctx);
        this.item = item;
        this.object = object;
        this.completed = completed;
    }

    @Override
    public boolean completed() {
        return completed.completed();
    }

    @Override
    public void run() {
        ctx.executor.offer(new OpenTab(ctx, Game.Tab.INVENTORY), new Interact(ctx, item, "Use", Interact.INVENTORY, new Completable() {
            @Override
            public boolean completed() {
                return ctx.inventory.select().name(item).peek().contains(ctx.mouse.getLocation()) &&  ctx.menu.items()[0].equals("Cancel");
            }
        }), new SleepTask(ctx, 500),
                new Interact(ctx, object, "Use", Interact.OBJECT, new Completable() {
                    @Override
                    public boolean completed() {
                        return !ctx.inventory.select().name(item).peek().contains(ctx.mouse.getLocation()) || ctx.players.local().animation() != -1;
                    }
                }, true), new SleepTask(ctx, 2500, completed));
    }
}
