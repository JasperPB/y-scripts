package org.totalrequiem.core.task;

import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.context.Provider;
import org.totalrequiem.core.impl.Completable;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public abstract class Task extends Provider implements Completable, Runnable {

    public Task(final Context ctx) {
        super(ctx);
    }

    @Override
    public String toString() {
        return this.getClass().getCanonicalName();
    }
}
