package org.totalrequiem.core.obs.list;

import java.util.EventListener;

/**
 * Created with IntelliJ IDEA.
 * User: JasperPC
 * Date: 8/6/14
 * Time: 2:16 PM
 * To change this template use File | Settings | File Templates.
 */
public interface VarpbitListener extends EventListener {

    /**
     * Gets called when a varpbit changes
     * @param varpbit the varpbit that changed
     * @param oldValue the value of this varpbit before the change
     * @param newValue the value of this varpbit after the change (current value)
     */
    public void varpbitChanged(final int varpbit, final int oldValue, final int newValue);

}
