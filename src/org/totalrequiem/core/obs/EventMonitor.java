package org.totalrequiem.core.obs;

import org.powerbot.script.rt4.Item;

import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.context.Provider;
import org.totalrequiem.core.obs.list.AnimationListener;
import org.totalrequiem.core.obs.list.InventoryListener;

import org.totalrequiem.core.obs.list.VarpbitListener;

import java.util.EventListener;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public class EventMonitor extends Provider {

    private final HashSet<EventListener> set = new HashSet<>();
    private final Thread[] monitors;

    public EventMonitor(final Context ctx) {
        super(ctx);
        monitors = new Thread[]{new Thread(new AnimationMonitor()),
                new Thread(new InventoryMonitor()), new Thread(new VarpbitsMonitor())};
    }

    public void offer(final EventListener e) {
        set.add(e);
    }

    private boolean active() {
        return !ctx.controller.isStopping();
    }

    public void start() {
        for (final Thread t : monitors) {
            t.start();
        }
    }

    public void kill() {
        for (Thread t : monitors) {
            t = null;
        }
    }

    private boolean hasListener(final Class<? extends EventListener> req) {
        for (final EventListener e : set) {
            if (e.getClass() == req)
                return true;
        }
        return false;
    }



    private class AnimationMonitor implements Runnable {

        @Override
        public void run() {
            int previous = ctx.players.local().animation();
            while (active()) {
                if (!hasListener(AnimationListener.class)) continue;
                int current = ctx.players.local().animation();
                if (previous != current) {
                    for (final EventListener listener : set) {
                        if (listener instanceof AnimationListener) {
                            ((AnimationListener) listener).animationChanged(previous, current);
                        }
                    }
                    previous = current;
                }
            }
        }
    }

    private class InventoryMonitor implements Runnable {

        @Override
        public void run() {
            final LinkedList<Item> items = new LinkedList<>();
            for (int i = 1; i <= 28; i++) {
                items.add(i - 1, ctx.inventory.itemAt(i));
            }

            while (active()) {
                for (int i = 1; i <= 28; i++) {
                    final Item item = ctx.inventory.itemAt(i);
                    final Item last = items.get(i - 1);
                    if (last.id() != item.id() || last.stackSize() != item.stackSize()) {
                        for (final EventListener e : set) {
                            if (e instanceof InventoryListener) {
                                ((InventoryListener) e).inventoryChanged(i, last, item);
                            }
                        }
                        items.set(i - 1, item);
                    }
                }
            }
        }
    }

    private class VarpbitsMonitor implements Runnable {

        @Override
        public void run() {
            int[] old = ctx.varpbits.array();
            while (active()) {
                if (!hasListener(VarpbitListener.class)) continue;
                int[] current = ctx.varpbits.array();
                for (int i = 0; i < current.length; i++) {
                    if (current[i] != old[i]) {
                        for (final EventListener e : set) {
                            if (e instanceof VarpbitListener) {
                                ((VarpbitListener) e).varpbitChanged(i, current[i], old[i]);
                            }
                        }
                        old[i] = current[i];
                    }
                }
            }
        }
    }

}
