package org.totalrequiem.core;

import org.totalrequiem.core.context.Context;
import org.totalrequiem.core.task.Task;

/**
 * @author Jasper
 * @version 1.0
 * @since 1.0
 */
public abstract class Action extends Task {


    public Action(final Context ctx) {
        super(ctx);
    }

    public abstract Task[] tasks();

    @Override
    public void run() {
        ctx.executor.offer(tasks());
    }

    @Override
    public String toString() {
        return this.getClass().getCanonicalName();
    }


}
